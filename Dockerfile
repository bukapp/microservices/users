FROM node:latest

WORKDIR /usr/src/app

COPY . . 

USER root

RUN npm install

RUN npm uninstall bcrypt

RUN npm install bcrypt

RUN npm run build

EXPOSE 3008

ENTRYPOINT [ "npm", "run", "start" ]
