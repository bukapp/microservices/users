import express, { Express, Request, Response } from 'express';
import bodyParser from 'body-parser'
import customerRouter from './routes/customer';
import employeeRouter from './routes/employee';
import mariadbConnection from './connections/mariadb';
import {crypto} from './modules/modules';
import jwt from 'jsonwebtoken'
import middleware from './modules/middleware'

const app: Express = express();
const port: Number = 3008;

app.set('db',mariadbConnection);
app.set('crypto', crypto);
app.set('jwt', jwt);
app.set('middleware', middleware)

app.use(bodyParser.urlencoded({ extended: false }));          
app.use(bodyParser.json());
app.use('/customers', customerRouter);
app.use('/employees', employeeRouter);
const server = app.listen(port, () => {
    console.log('[server]: The server is running at http://localhost:' + port);
});

export const get = () => {return app};
export const connection = () => {return server}
export const pool = () => {return mariadbConnection}
