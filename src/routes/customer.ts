import express, { Express, Request, Response, Router, NextFunction } from 'express';
import middleware from '../modules/middleware';
var router: Router = express.Router()

/* POST a user */
router.post('/', async function (req: Request, res: Response, next: NextFunction) {
    try {
        const db = req.app.get('db');
        const exists = await db.pool.query("select * from customers where email = ?", [req.body.email])
        console.log(exists)
        if(exists.length > 0){
            res.status(400).send({error: "Email taken"})
        }

        const crypto = req.app.get('crypto')

        const pswd = await crypto.cryptPassword(req.body.password)
        const results = await db.pool.query("insert into customers (email, first_name, last_name, address, password, phone_number, mentoring_code, role) values (?,?,?,?,?,?,?,?)",
            [req.body.email,
            req.body.first_name,
            req.body.last_name,
            req.body.address,
            pswd,
            req.body.phone_number,
            req.body.mentoring_code || "",
                '["CUSTOMER"]'
            ])
        console.log(results)
        res.sendStatus(204)
    }  catch (error) {
        console.log(error)
        res.status(500).send({ error: "Internal error" })
    }
});

/* LOGIN */
router.post('/login', async function (req: Request, res: Response, next: NextFunction) {
    try{
        const db = req.app.get('db');
        const crypto = req.app.get('crypto');
        const jwt = req.app.get('jwt');

        const results = await db.pool.query('select * from customers where customers.email = ?', [req.body.email]);

        if(results.length <= 0) {
            res.sendStatus(404)
            return;
        }
        if(await crypto.comparePassword(req.body.password, results[0].password)){
            const token = jwt.sign(results[0], process.env.CUSTOMER_SECRET_KEY, { expiresIn: process.env.TOKEN_LIFE})
            res.send(token)
            return;
        }
        res.sendStatus(403)
    }catch (error) {
        console.log(error)
        res.status(500).send({ error: "Internal error" })
    }
});


/**** SECURE ZONE ****/
router.use(middleware)

/* GET all users */
router.get('/', async function (req: Request, res: Response, next: NextFunction) {
    try {
        const db = req.app.get('db');
        const results = await db.pool.query('select * from customers')
        res.send(results)
    } catch (error) {
        console.log(error)
        res.status(500).send({ error: "Internal error" })
    }
});

/* GET current user */
router.get('/:id', async function (req: Request, res: Response, next: NextFunction) {
    try {
        const db = req.app.get('db');
        const results = await db.pool.query('select * from customers where customers.id = ?', [req.params.id])
        if (results.length <= 0) {
            res.sendStatus(404);
            return
        }
        res.send(results[0]);
    }  catch (error) {
        console.log(error)
        res.status(500).send({ error: "Internal error" })
    }
});

/* UPDATE a user */
router.put('/', async function (req: Request, res: Response, next: NextFunction) {
    try {
        const db = req.app.get('db');
        const crypto = req.app.get('crypto')
        const results = await db.pool.query('select * from customers where customers.email = ?', [req.body.user.email])
        if (results.length <= 0) {
            res.sendStatus(404);
            return
        }

        const pswd = req.body.password ? await crypto.cryptPassword(req.body.password) : results[0].password

        await db.pool.query("update customers set email = ?, first_name = ?, last_name = ?, address = ?, password = ?, phone_number = ? where id = ?",
            [req.body.email || results[0].email,
            req.body.first_name || results[0].first_name,
            req.body.last_name || results[0].last_name,
            req.body.address || results[0].address,
            pswd,
            req.body.phone_number || results[0].phone_number,
            results[0].id
            ]);

        res.sendStatus(204);

    } catch (error) {
        console.log(error)
        res.status(500).send({ error: "Internal error" })
    }
});

/* DELETE a user */
router.delete('/:id', async function (req: Request, res: Response, next: NextFunction) {
    try {
        const db = req.app.get('db');
        const user = await db.pool.query('select * from customers where customers.id = ?', [req.params.id])

        if(req.body.user.email == user[0].email){

            const results = await db.pool.query('delete from customers where customers.id = ?', [req.params.id])

            if (results.affectedRows <= 0) {
                res.sendStatus(404);
                return;
            }
            res.sendStatus(204);

        } else res.sendStatus(403);
    }  catch (error) {
        console.log(error)
        res.status(500).send({ error: "Internal error" })
    }
});

export default router;